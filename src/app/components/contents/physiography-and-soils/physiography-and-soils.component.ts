import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-physiography-and-soils',
  templateUrl: './physiography-and-soils.component.html',
  styleUrls: ['./physiography-and-soils.component.css', '../shared.css'],
  animations: [
    trigger('changeScale', [
      state('close', style({
        opacity: 0
      })),
      state('open', style({
        opacity: 1
      })),
      transition('close=>open', animate('500ms')),
      transition('open=>close', animate('500ms'))
    ])
  ]
})
export class PhysiographyAndSoilsComponent implements OnInit {
  @Input() currentState: string;
  constructor() { }

  ngOnInit(): void { }

}
