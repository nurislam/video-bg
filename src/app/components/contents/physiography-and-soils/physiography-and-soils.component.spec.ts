import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysiographyAndSoilsComponent } from './physiography-and-soils.component';

describe('PhysiographyAndSoilsComponent', () => {
  let component: PhysiographyAndSoilsComponent;
  let fixture: ComponentFixture<PhysiographyAndSoilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysiographyAndSoilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiographyAndSoilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
