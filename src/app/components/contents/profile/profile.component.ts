import {Component, OnInit, Input} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {trigger, state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../shared.css'],
  animations: [
    trigger('changeScale', [
      state('close', style({
        opacity: 0
      })),
      state('open', style({
        opacity: 1
      })),
      transition('close=>open', animate('500ms')),
      transition('open=>close', animate('500ms'))
    ])
  ]
})
export class ProfileComponent implements OnInit {
  // data = '';
  @Input() currentState: string;

  // constructor(
  //   private http: HttpClient
  // ) {
  // }

  ngOnInit(): void {
    // const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    // this.http.get('./assets/contents/door22.html', {headers, responseType: 'text'}).subscribe(data => {
    //   this.data = data;
    // });
  }

}
