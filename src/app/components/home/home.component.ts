import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output,
  AfterViewInit,
  ChangeDetectorRef,
  AfterContentInit
} from '@angular/core';

declare var $: any;
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

const vConfig = {
  door11: {
    open_start: 0,
    open_end: 2000,
    close_start: 32.40,
    close_end: 2600
  },
  door21: {
    open_start: 2.0,
    open_end: 3700,
    close_start: 5.70,
    close_end: 3500
  },
  door22: {
    open_start: 9.20,
    open_end: 4200,
    close_start: 13.40,
    close_end: 4000
  },
  door23: {
    open_start: 17.40,
    open_end: 3700,
    close_start: 21.10,
    close_end: 3500
  },
  door24: {
    open_start: 24.60,
    open_end: 4000,
    close_start: 28.60,
    close_end: 3800
  }
};

const aConfig = {
  door11: 'assets/resources/audio/main.mp3',
  door21: 'assets/resources/audio/physiography_and_soils_gallery.mp3',
  door22: 'assets/resources/audio/profile_gallery.mp3',
  door23: 'assets/resources/audio/series_gallery.mp3',
  door24: 'assets/resources/audio/map_gallery.mp3'
};

const sleep = (ms) => {
  const end = +(new Date()) + ms;
  while (+(new Date()) < end) {
  }
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('myVideo') video: ElementRef; // Prior to Angular 8
  @ViewChild('myAudio') audio: ElementRef; // Prior to Angular 8
  @Output() closedoorfire: EventEmitter<any> = new EventEmitter<any>();
  @Output() secondview: EventEmitter<any> = new EventEmitter<any>();
  @Output() firstdoorview: EventEmitter<any> = new EventEmitter<any>();
  @Output() changedoorstate: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeLevel: EventEmitter<any> = new EventEmitter<any>();

  door11 = false;
  second = false;
  doorCloseId = '';
  doorContents = {
    door11: false,
    door21: false,
    door22: false,
    door23: false,
    door24: false
  };
  level = 1;
  doorState = {
    door11: 'close',
    door21: 'close',
    door22: 'close',
    door23: 'close',
    door24: 'close'
  };

  constructor(
    private cdr: ChangeDetectorRef,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit(): void {
    this.secondview.subscribe(async (data) => {
      if (!data.second) {
        $('.second').fadeOut();
        await this.delay(500);
      }
      this.second = data.second;
    });
    this.closedoorfire.subscribe(async (data) => {
      if (data.doorCloseId === '') {
        $('.door-close').fadeOut();
        await this.delay(500);
      }
      this.doorCloseId = data.doorCloseId;
    });
    this.firstdoorview.subscribe((data) => {
      this.door11 = data.door11;
    });
    this.changedoorstate.subscribe((data) => {
      this.doorState[data.id] = data.state;
    });
    this.changeLevel.subscribe((data) => {
      this.level = data.level;
    });
  }

  ngAfterViewInit() {
    var firstdoorview = this.firstdoorview;

    this.video.nativeElement.poster = 'assets/resources/bg-poster.jpg';

    var video = this.video;
    var request = new XMLHttpRequest();
    request.open('GET', 'assets/resources/bg.mp4', true);
    request.responseType = 'blob';
    request.onload = function() {
      var reader = new FileReader();
      reader.readAsDataURL(request.response);
      reader.onload = function(e) {
        let blobsrc = e.target.result;
        // console.log(blobsrc);
        video.nativeElement.src = blobsrc;
        setTimeout(function() {
          firstdoorview.emit({door11: true});
        }, 1000);
      };
    };
    request.send();

  }

  async door11open() {
    // console.log('door11 is opening');
    this.video.nativeElement.currentTime = vConfig.door11.open_start;

    $('.second').fadeOut();
    await this.delay(500);
    this.door11 = false;
    this.level = 2;
    var video = this.video;
    do {
      video.nativeElement.play();
    } while (!video.nativeElement.canPlayType);
    {
      video.nativeElement.play();
    }

    this.audio.nativeElement.src = aConfig.door11;
    this.audio.nativeElement.play();

    var secondview = this.secondview;

    var closedoorfire = this.closedoorfire;
    setTimeout(function() {
      video.nativeElement.pause();
      secondview.emit({second: true});
      closedoorfire.emit({doorCloseId: 'door11'});
    }, vConfig.door11.open_end);
  }

  async door2Open(id) {
    // console.log(id + ' is opening');
    $('.second').fadeOut();
    await this.delay(500);
    this.second = false;
    this.doorCloseId = '';
    this.video.nativeElement.currentTime = vConfig[id].open_start;
    this.video.nativeElement.play();

    // console.log(aConfig[id] + ' is playing');
    this.audio.nativeElement.src = aConfig[id];
    this.audio.nativeElement.play();

    var video = this.video;
    this.doorContents[id] = true;
    this.doorState[id] = 'close';

    var closedoorfire = this.closedoorfire,
      changedoorstate = this.changedoorstate;
    setTimeout(function() {
      video.nativeElement.pause();
      video.nativeElement.currentTime = vConfig[id].open_start + (vConfig[id].open_end / 1000);
      closedoorfire.emit({doorCloseId: id});
      changedoorstate.emit({id: id, state: 'open'});
    }, vConfig[id].open_end);
  }

  async doorClose() {
    this.video.nativeElement.pause();
    var id = this.doorCloseId;
    // console.log(id + ' is closing');

    this.doorState[id] = 'close';
    $('.door-close').fadeOut();

    var video = this.video;
    if (id === 'door11') {
      $('.door-title').fadeOut();
      await this.delay(500);
      this.second = false;
    } else {
      await this.delay(500);
    }
    this.doorContents[id] = false;

    this.doorCloseId = '';
    this.video.nativeElement.currentTime = vConfig[id].close_start;
    this.video.nativeElement.play();

    // if (id === 'door11') {
    //   this.audio.nativeElement.pause();
    //   this.audio.nativeElement.currentTime = 0;
    // } else {
    //   // console.log(aConfig[id] + ' is playing');
    //   this.audio.nativeElement.src = aConfig.door11;
    //   this.audio.nativeElement.play();
    // }
    this.audio.nativeElement.pause();
    this.audio.nativeElement.currentTime = 0;

    var secondview = this.secondview,
      firstdoorview = this.firstdoorview,
      closedoorfire = this.closedoorfire,
      changeLevel = this.changeLevel,
      level = this.level;
    setTimeout(function() {
      video.nativeElement.pause();
      video.nativeElement.currentTime = vConfig[id].close_start + (vConfig[id].close_end / 1000);
      if (id === 'door11') {
        changeLevel.emit({level: 1});
        level = 1;
      }

      if (level === 2) {
        secondview.emit({second: true});
        closedoorfire.emit({doorCloseId: 'door11'});
      }
      if (id === 'door11') {
        firstdoorview.emit({door11: true});
      }
    }, vConfig[id].close_end);
  }

  delay(ms) {
    console.log('Wait for ' + ms + 'ms');
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(ms);
      }, ms);
    });
  }
}
