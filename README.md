## Run
You’ll start by following this instruction to run the project.
```
1. Install Angular 9
2. npm install
3. ng serve
```

## Build
To create distribution need to run bellow script
```
ng build --prod
```
---
